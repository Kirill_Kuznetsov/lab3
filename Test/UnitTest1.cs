﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Lab1;
using MathNet.Numerics.LinearAlgebra;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestSystemOne()
        {
            double[,] A = new double[,] {{ 15.5, -13.1, 37 }, { 40.1, 7.2, -11.9 }, { 11.5, 54.8, 23.7 }};
            double[] B = new double[] { 16.79, -53.23, 141.57 };
            double[] Expected = new double[] {-1.2, 2.1, 1.7};

            Calculation с = new Calculation();
            double[] res = с.SolveSystem(A,B);

            CollectionAssert.AreEqual(Expected, res);
        }

        [TestMethod]
        public void TestSystemTwo()
        {
            double[,] A = new double[,] { {11.5, 54.8, 23.7 }, {15.5, -13.1, 47 }, {40.1, 7.2, -11.9} };
            double[] B = new double[] {141.57, 33.79, -53.23  };
            double[] Expected = new double[] { -1.2, 2.1, 1.7 };

            Calculation с = new Calculation();
            double[] res = с.SolveSystem(A, B);

            CollectionAssert.AreEqual(Expected, res);
        }

        [TestMethod]
        public void TestSystemThird()
        {
            double[,] A = new double[,] { { 2.9, 23.4, -17.4 }, { 12.9, 3.6, 0.7 }, { 221.9, 32.4, -37.4 } };
            double[] B = new double[] { -324.43, 5.45, -320.33 };
            double[] Expected = new double[] { 1.3, -5.4, 11.6 };

            Calculation с = new Calculation();
            double[] res = с.SolveSystem(A, B);

            CollectionAssert.AreEqual(Expected, res);
        }
    }
}
