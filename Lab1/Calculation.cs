﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;


namespace Lab1
{
    public class Calculation: INumericalIntegration
    {
        public double[] SolveSystem(double[,] A, double[] B)
        {
            Matrix<double> matrix = Matrix<double>.Build.DenseOfArray(A);

            double[] x = matrix.Solve(DenseVector.Build.DenseOfArray(B)).ToArray();

            for( int i=0; i < x.Length; i++)
                x[i] = Math.Round(x[i], 10);

            return x;
        }
    }
}
