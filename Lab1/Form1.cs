﻿using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Symbolics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Lab1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private double[] getCoef(string s)
        {
            Regex getIndices = new Regex(@"[^(x|y|z)+=\s]*");
            double[] arr = new double[4];
            int count = 0;

            if (getIndices.Matches(s).Count > 0)
            {
                foreach (Match match in getIndices.Matches(s))
                {
                    if (match.Value != "")
                    {
                        arr[count] = Convert.ToDouble(match.Value);
                        count++;
                    }
                }
            }

            return arr;
        }

        private double[] Matrix() 
        {
            string eq1 = equation1.Text;
            string eq2 = equation2.Text;
            string eq3 = equation3.Text;

            double[,] A = new double[3,3];
            double[] B = new double[3];

            double[] first = getCoef(eq1);
            double[] second = getCoef(eq2);
            double[] third = getCoef(eq3);

            for (int a = 0; a < first.Length; a++)
            {
                if (a < 3)
                {
                    A[0, a] = first[a];
                    A[1, a] = second[a];
                    A[2, a] = third[a];
                }
                else
                {
                    B[0] = first[a];
                    B[1] = second[a];
                    B[2] = third[a];
                }
            }

            Calculation с = new Calculation();
            double []x = с.SolveSystem(A,B);

            return x;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double[] res = Matrix();

            x.Text = Convert.ToString(res[0]);
            y.Text = Convert.ToString(res[1]);
            z.Text = Convert.ToString(res[2]);
        }
    }
}
