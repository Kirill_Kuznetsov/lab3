﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public interface INumericalIntegration
    {
        double[] SolveSystem(double[,] A, double[] B);

    }
}
