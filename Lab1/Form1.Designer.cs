﻿
namespace Lab1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.equation2 = new System.Windows.Forms.TextBox();
            this.equation1 = new System.Windows.Forms.TextBox();
            this.x = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.equation3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.z = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.y = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(224, 147);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(74, 20);
            this.button1.TabIndex = 8;
            this.button1.Text = "Рассчитать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 88.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(118, -5);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 134);
            this.label6.TabIndex = 11;
            this.label6.Text = "{";
            // 
            // equation2
            // 
            this.equation2.Location = new System.Drawing.Point(184, 65);
            this.equation2.Margin = new System.Windows.Forms.Padding(2);
            this.equation2.Name = "equation2";
            this.equation2.Size = new System.Drawing.Size(157, 20);
            this.equation2.TabIndex = 5;
            this.equation2.Text = "40,1x+7,2y-11,9z = -53,23";
            // 
            // equation1
            // 
            this.equation1.Location = new System.Drawing.Point(184, 31);
            this.equation1.Margin = new System.Windows.Forms.Padding(2);
            this.equation1.Name = "equation1";
            this.equation1.Size = new System.Drawing.Size(157, 20);
            this.equation1.TabIndex = 4;
            this.equation1.Text = "15,5x-13,1y+37z = 16,79";
            // 
            // x
            // 
            this.x.Location = new System.Drawing.Point(387, 31);
            this.x.Margin = new System.Windows.Forms.Padding(2);
            this.x.Name = "x";
            this.x.Size = new System.Drawing.Size(39, 20);
            this.x.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(365, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Результат:";
            // 
            // equation3
            // 
            this.equation3.Location = new System.Drawing.Point(184, 99);
            this.equation3.Margin = new System.Windows.Forms.Padding(2);
            this.equation3.Name = "equation3";
            this.equation3.Size = new System.Drawing.Size(157, 20);
            this.equation3.TabIndex = 7;
            this.equation3.Text = "11,5x+54,8y+23,7z = 141,57";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(365, 34);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "х=";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(365, 102);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "z=";
            // 
            // z
            // 
            this.z.Location = new System.Drawing.Point(387, 99);
            this.z.Margin = new System.Windows.Forms.Padding(2);
            this.z.Name = "z";
            this.z.Size = new System.Drawing.Size(39, 20);
            this.z.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(365, 68);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "y=";
            // 
            // y
            // 
            this.y.Location = new System.Drawing.Point(387, 65);
            this.y.Margin = new System.Windows.Forms.Padding(2);
            this.y.Name = "y";
            this.y.Size = new System.Drawing.Size(39, 20);
            this.y.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1033, 366);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.y);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.z);
            this.Controls.Add(this.equation3);
            this.Controls.Add(this.equation2);
            this.Controls.Add(this.equation1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.x);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox equation2;
        private System.Windows.Forms.TextBox equation1;
        private System.Windows.Forms.TextBox x;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox equation3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox z;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox y;
    }
}

